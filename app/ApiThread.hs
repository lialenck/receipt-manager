{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module ApiThread (apiThread) where

import Conduit
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TChan
import Control.Concurrent (threadDelay)
import Control.Monad (when, forever, forM_)
import Control.Monad.Logger
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader (ReaderT)
import Database.Persist.Sqlite
import Data.Time.Calendar (toGregorian, fromGregorian, addGregorianMonthsClip)
import Data.Time.Clock (getCurrentTime, UTCTime(..), diffUTCTime)

import qualified Data.Text as T

import ReceiptOcr
import ReceiptStorage

sleepNextMonth :: IO ()
sleepNextMonth = do
    now <- getCurrentTime
    let day                          = utctDay now
        (wantedYear, wantedMonth, _) = toGregorian $ addGregorianMonthsClip 1 day
        wantedDay                    = fromGregorian wantedYear wantedMonth 1
        toSleep                      = UTCTime wantedDay 0 `diffUTCTime` now
    threadDelay $ 1000000 * (ceiling $ realToFrac toSleep)

sleepIfRequired
    :: ReceiptOcrBackend ocr
    => ocr
    -> ConnectionPool
    -> LoggingT IO ()
sleepIfRequired bk pool = case requestsPerMonth bk of
    Nothing -> return ()
    Just lim -> do
        n <- runSqlPool (getUsage $ backendName bk) pool
        when (n >= lim) $ do
            $(logInfo) $ "Rate limit reached for "
                <> T.pack (backendName bk) <> "; sleeping until next month"
            liftIO sleepNextMonth
            $(logInfo) "Resuming ocr recognition after long slumber."

processId
    :: ReceiptOcrBackend ocr
    => ocr
    -> ConnectionPool
    -> ReceiptImageId
    -> MaybeT (LoggingT IO) ()
processId backend pool i = do
    let runSql :: (MonadUnliftIO m) => ReaderT SqlBackend m a -> m a
        runSql action = runSqlPool action pool
        bkPref = T.pack (backendName backend) <> ": "

    lift $ sleepIfRequired backend pool

    ReceiptImage name mime blob _ <- MaybeT $ runSql $ get i
    $(logInfo) $ "Received image: " <> T.pack name

    if mime `notElem` supportedMimeTypes backend
    then do
        $(logError) $ bkPref <> "Unsupported mime type: " <> T.pack mime
        fail ""
    else return ()

    -- assume we should increase recorded API usage even if performOcr fails
    liftIO $ runSql $ incUsage $ backendName backend
    res <- liftIO $ performOcr backend mime blob

    (meta, entries) <- case res of
        OcrResult a -> return a
        OcrError s -> do
            $(logError) $ bkPref <> "Api Error: " <> T.pack s
            fail ""
        OcrRaw bs -> do
            -- lift instead of MaybeT because we want to ignore errors
            lift $ runSql (error "TODO" ) >>= \case
                Just _ -> return ()
                Nothing -> $(logWarn)
                    $ bkPref <> "Api answer exists for " <> T.pack name

            case processAnswer backend bs of
                Right x -> return x
                Left s -> do
                    $(logError) $ bkPref <> "Api processing error: " <> T.pack s
                    fail ""

    MaybeT $ runSql $ insertUnique $ MetaAuto i meta
    forM_ entries $ \e ->
        MaybeT $ runSql $ insertUnique $ EntryAuto i e

apiThread
    :: ReceiptOcrBackend ocr
    => ConnectionPool
    -> TChan ReceiptImageId
    -> ocr
    -> LoggingT IO ()
apiThread pool tc backend =
    let getNewIds :: ConduitT a ReceiptImageId (LoggingT IO) ()
        getNewIds = do
            yieldMany       =<< liftIO (runSqlPool getUnrecognized pool)
            forever $ yield =<< liftIO (atomically $ readTChan tc)

        processIds = mapM_C $ \i -> do
            runMaybeT $ processId backend pool i
            return ()

    in runConduit $ getNewIds .| processIds
