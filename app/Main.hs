{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}

module Main (main) where

import Conduit
import Control.Concurrent (forkIO)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TChan
import Control.Monad (when)
import Crypto.Hash.Algorithms (SHA512)
import Crypto.Hash.Conduit
import Crypto.Hash (Digest)
import Data.Aeson
import Database.Persist.Sqlite
import Data.ByteArray (convert)
import Data.Functor ((<&>))
import Network.HTTP.Types.Status (badRequest400, unauthorized401)
import System.Posix.Directory (changeWorkingDirectory)
import Yesod

import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy as BL

import ReceiptOcr.Dummy
import ReceiptStorage
import SignalLogger
import ApiThread

data ReceiptManager = ReceiptManager {
    getSqlPool :: ConnectionPool,
    getImgChan :: TChan ReceiptImageId
}

mkYesod "ReceiptManager" [parseRoutes|

/                   HomeR         GET
/upload-img/#String UploadImgR    POST
/unverified-imgs    UnverifImgsR  GET

|]

instance Yesod ReceiptManager where
    maximumContentLength _ _ = Just $ 10 * 1024^2 -- 10M

instance YesodPersist ReceiptManager where
    type YesodPersistBackend ReceiptManager = SqlBackend

    runDB action = getYesod >>= runSqlPool action . getSqlPool

requireAuth :: ToTypedContent a => a -> Handler ()
requireAuth r = do
    lookupHeader "authentication" >>= \case
        Just "TODO wubbel wubbel" -> return ()
        _                         -> sendResponseStatus unauthorized401 r

getHomeR :: Handler String
getHomeR = return "henlo"

postUploadImgR :: String -> Handler Value
postUploadImgR im = do
    requireAuth $ toTypedContent $ object ["error" .= ("Login failed" :: String)]

    imgType <- lookupHeader "content-type" >>= \case
        Nothing -> sendResponseStatus badRequest400
            $ object ["error" .= ("missing content-type" :: String)]
        Just t -> return t

    when (not $ any (imgType `BS.isPrefixOf`) ["image/png", "image/jpeg", "image/webp"])
        $ sendResponseStatus badRequest400
        $ object ["error" .= ("Unsupported image type" :: String)]

    (bss, digest) <- runConduit $ rawRequestBody
        .| getZipSink ((,) <$> ZipSink sinkList <*> ZipSink sinkHash)

    let blob = BL.toStrict $ BL.fromChunks bss
        hash = digest :: Digest SHA512

    b <- runDB $ insertUnique $ ReceiptImage im (B8.unpack imgType) blob $ convert hash
    case b of
        Just i -> getYesod >>= liftIO . atomically . flip writeTChan i . getImgChan
        Nothing -> sendResponseStatus badRequest400
            $ object ["error" .= ("Image exists" :: String)]

    return $ object ["error" .= Null]

getUnverifImgsR :: Handler Value
getUnverifImgsR = do
    requireAuth $ toTypedContent $ object ["error" .= ("Login failed" :: String)]

    -- TODO this shouldn't just be a list of opaque IDs, but a list of objects
    -- containing the data to be verified (except for maybe the images)
    runDB getUnverified <&> \imgs ->
        object ["error" .= Null, "imgs" .= map show imgs]

main :: IO ()
main = do
    changeWorkingDirectory "data"

    runSignalLogger $ withReceiptDBPool $ \pool -> liftIO $ do
        tc <- atomically newTChan

        forkIO $ runSignalLogger $ apiThread pool tc DummyOcr
        warp 18025 $ ReceiptManager pool tc -- might need some CORS later
