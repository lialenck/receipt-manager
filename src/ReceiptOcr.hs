{-# LANGUAGE TypeFamilies #-}

module ReceiptOcr (
    module ReceiptOcr.Types,

    ReceiptOcrBackend(..)
) where

import Data.ByteString (ByteString)

import ReceiptOcr.Types

class ReceiptOcrBackend a where
    -- a is only in the signature so we know which instance of ReceiptOcrBackend we're
    -- asking for these values
    backendName        :: a -> String
    supportedMimeTypes :: a -> [String]
    requestsPerMonth   :: a -> Maybe Int
    -- default: no limit
    requestsPerMonth _ = Nothing

    -- backends may only be partially implemented in haskell; in that case, we might
    -- get some response from an external api, and parse that in haskell.
    -- For those cases, we want to cache that response in the database, in case we have
    -- a bug in our code parsing it, and want to report offending api responses.
    -- or cache the response to process with updated haskell code.
    -- An additional reason for that might be storing info that we don't know how to
    -- parse yet.
    --
    -- For all of these reasons, instances may produce a bytestring from performOcr,
    -- which is stored in the database, and then passed to processAnswer for parsing.
    processAnswer :: a -> ByteString -> Either String (Meta, [Entry])
    processAnswer a _ = Left $ "processAnswer not implemented for " ++ backendName a

    performOcr :: a -> String -> ByteString -> IO OcrResult
