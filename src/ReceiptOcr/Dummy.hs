module ReceiptOcr.Dummy (DummyOcr(..)) where

import ReceiptOcr

data DummyOcr = DummyOcr deriving Show

instance ReceiptOcrBackend DummyOcr where
    backendName _ = "Dummy"
    supportedMimeTypes _ = ["image/png", "image/jpeg", "image/webp", "image/avif"]

    performOcr _ _ _ = return $! OcrError "TODO"
