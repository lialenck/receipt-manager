module ReceiptOcr.Types (
    Meta(..),
    Entry(..),
    OcrResult(..)
) where

import Data.ByteString (ByteString)
import Data.Time (UTCTime)

-- TODO this could possibly use more fields
data Meta = Meta UTCTime deriving (Show, Read, Eq)

data Entry = Entry {
    entryName :: String,
    entryPrice :: Int, -- cents
    entryCount :: Int
} deriving (Show, Read, Eq)

data OcrResult = OcrError String | OcrRaw ByteString | OcrResult (Meta, [Entry])
    deriving (Show, Read, Eq)
