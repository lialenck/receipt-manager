{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module ReceiptStorage (
    module ReceiptStorage.Types,

    withReceiptDBPool,
    getUnverified,
    getUnrecognized,
    getUsage,
    incUsage,
) where

import Control.Monad.IO.Unlift
import Control.Monad.Logger (MonadLoggerIO)
import Control.Monad.Trans.Reader (ReaderT)
import Database.Persist.Sqlite
import Data.Functor ((<&>))
import Data.Time.Calendar (toGregorian)
import Data.Time.Clock (getCurrentTime, UTCTime(..))
import qualified Data.HashSet as S

import ReceiptOcr.Types
import ReceiptStorage.Types

withReceiptDBPool :: (MonadUnliftIO m, MonadLoggerIO m) => (ConnectionPool -> m a) -> m a
withReceiptDBPool f = withSqlitePool "data.db" 4 $ \pool -> do
    runSqlPool (runMigration migrateAll) pool
    f pool

getUnverified
    :: (MonadUnliftIO m,
        PersistQueryRead backend,
        PersistRecordBackend MetaAuto backend,
        PersistRecordBackend MetaMan backend)
    => ReaderT backend m [ReceiptImageId]
getUnverified = fmap S.toList $
    S.difference
    <$> fmap (S.fromList . map (metaAutoReceipt . entityVal)) (selectList [] [])
    <*> fmap (S.fromList . map (metaManReceipt  . entityVal)) (selectList [] [])

getUnrecognized
    :: (MonadUnliftIO m,
        PersistQueryRead backend,
        PersistRecordBackend ReceiptImage backend,
        PersistRecordBackend MetaAuto backend)
    => ReaderT backend m [ReceiptImageId]
getUnrecognized = fmap S.toList $
    S.difference
    <$> fmap (S.fromList . map  entityKey                    ) (selectList [] [])
    <*> fmap (S.fromList . map (metaAutoReceipt  . entityVal)) (selectList [] [])

getUsage
    :: (MonadUnliftIO m,
        PersistUniqueRead backend,
        PersistRecordBackend MonthQueries backend)
    => String
    -> ReaderT backend m Int
getUsage ocrType = do
    (year, month, _) <- toGregorian . utctDay <$> liftIO getCurrentTime

    getBy (UniqueDate ocrType (fromIntegral year) month) <&> \case
        Just x -> monthQueriesCount $ entityVal x
        Nothing -> 0

incUsage
    :: (MonadUnliftIO m,
        PersistUniqueWrite backend,
        PersistRecordBackend MonthQueries backend)
    => String
    -> ReaderT backend m ()
incUsage ocrType = do
    (year_, month, _) <- toGregorian . utctDay <$> liftIO getCurrentTime
    let year = fromIntegral year_

    -- increase by 1, or insert 1 if nonexistent
    upsertBy (UniqueDate ocrType (fromIntegral year) month)
        (MonthQueries ocrType (fromIntegral year) month 1) [MonthQueriesCount +=. 1]
    return ()
