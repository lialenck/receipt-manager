{-# OPTIONS_GHC -Wno-name-shadowing #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ViewPatterns #-}

module ReceiptStorage.Types where

--module ReceiptStorage.Types (
--    ReceiptImage(..),
--    EntryAuto(..),
--    MetaAuto(..),
--    EntryMan(..),
--    MetaMan(..),
--    MonthQueries(..),
--
--    ReceiptImageId,
--    EntryAutoId,
--    MetaAutoId,
--    EntryManId,
--    MetaManId,
--    MonthQueriesId,
--
--    UniqueImgHash,
--    UniqueMetaAuto,
--    UniqueMetaMan,
--    UniqueDate,
--    UniqueRawResult,
--
--    migrateAll
--) where

import Database.Persist
import Database.Persist.TH
import Data.Hashable
import Data.Time
import GHC.Generics hiding (Meta)
import Data.ByteString (ByteString)

import ReceiptOcr.Types

derivePersistField "Meta"
derivePersistField "Entry"

-- TODO is it correct that this produces an absurd amount of -Wname-shadowing warnings?
-- does it even still do that?
share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
ReceiptImage
    name String
    -- could use an enum type, but this is more handy.
    -- it did cause a bug due to me swapping name/mime on accident though.
    mime String
    blob ByteString
    hash ByteString

    UniqueImgHash hash

-- what the algorithm thinks
EntryAuto
    receipt ReceiptImageId
    content Entry
MetaAuto
    receipt ReceiptImageId
    content Meta

    UniqueMetaAuto receipt

-- what we got from someone manually checking
EntryMan
    receipt ReceiptImageId
    content Entry
MetaMan
    receipt ReceiptImageId
    content Meta

    UniqueMetaMan receipt

-- we might want to limit api usage, so store usage information
-- for that, we could introduce an enum type to describe the api type; however,
-- that would make using generic backends harder in other places.
MonthQueries
    apiType String
    year  Int
    month Int
    count Int

    UniqueDate apiType year month

RawApi
    receipt ReceiptImageId
    apiType String
    content ByteString

    UniqueRawResult receipt
|]

-- taken from
-- https://stackoverflow.com/questions/23976276/hashing-by-persistents-entity-key
deriving instance Generic LiteralType
instance Hashable LiteralType
deriving instance Generic ZonedTime
instance Hashable ZonedTime
deriving instance Generic PersistValue
instance Hashable PersistValue

instance (PersistEntity a) => Hashable (Key a) where
    hashWithSalt s k = hashWithSalt s $ keyToValues k
