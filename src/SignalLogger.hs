module SignalLogger (runSignalLogger) where

import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Logger
import System.IO (stderr)

sendSignalMessage :: LogStr -> IO ()
sendSignalMessage s =
    let bs = fromLogStr s
    in return ()

signalLogger :: Loc -> LogSource -> LogLevel -> LogStr -> IO ()
signalLogger loc src lvl msg = case lvl of
    LevelDebug -> return ()
    LevelInfo -> defaultOutput stderr loc src lvl msg
    _ -> do
        defaultOutput stderr loc src lvl msg
        sendSignalMessage $ defaultLogStr loc src lvl msg

-- TODO implement a logger that sends messages with high level on signal
runSignalLogger :: MonadIO m => LoggingT m a -> m a
runSignalLogger m = runLoggingT m signalLogger
